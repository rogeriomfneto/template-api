const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  api.post("/users", postUser(stanConn, mongoClient));
  api.delete("/users/:uuid", deleteUser(stanConn, secret));

  return api;
};

const deleteUser = (stanConn, secret) => (req, res) => {
  const id = req.params.uuid;
  const authenticationHeader = req.headers.authentication;
  if (authenticationHeader == undefined) {
    return res.status(401).json({
      "error": "Access Token not found"
    });
  }

  if (!matchId(id, authenticationHeader, secret)) {
    return res.status(403).json({
        "error": "Access Token did not match User ID"
    })
  }

  const natsPayload = JSON.stringify({
    "eventType": "UserDeleted",
    "entityId": id,
    "entityAggregate": {}
  });

  stanConn.publish("UserDeleted", natsPayload, (err, guid) => {
    if (err) {
      console.log("publish failed: " + err);
    } else {
      console.log("published message with guid: " + guid);
    }
  });

  return res.status(200).json({
    "id": id
  })
}

const matchId = (id, authenticationHeader, secret) => {
  const token = authenticationHeader.split(" ")[1];
  try {
    const decodedObject = jwt.verify(token, secret);
    console.log('decodedObject', decodedObject)
    const authenticationId = decodedObject.id;
    console.log("id", id);
    console.log("authenticationId", authenticationId)
    return id == authenticationId;
  } catch (err) {
    console.log("Error decoding jwt", err);
    return false;
  }
}

const postUser = (stanConn, mongoClient) => async (req, res) => {
  const db = mongoClient.db("CRUD");
  const body = req.body;

  let field_name = getPostUserMissingField(body)
  if (field_name !== "") {
    return res.status(400).json({
      "error": `Request body had missing field ${field_name}`
    });
  };

  field_name = await getPostUserWrongType(body, db)
  if (field_name !== "") {
    return res.status(400).json({
      "error": `Request body had malformed field ${field_name}`
    });
  };

  if (!passwordAndConfirmationMatch(body)) {
    return res.status(422).json({
      "error": "Password confirmation did not match"
    });
  }
  
  const id = uuid();
  
  const natsPayload = JSON.stringify({
    "eventType": "UserCreated",
    "entityId": id,
    "entityAggregate": {
      "name": body.name,
      "email": body.email,
      "password": body.password,
    }
  });
  
  stanConn.publish("UserCreated", natsPayload, (err, guid) => {
    if (err) {
      console.log("publish failed: " + err);
    } else {
      console.log("published message with guid: " + guid);
    }
  });

  return res.status(201).json({
    "user": {
      "id": id,
      "name": body.name,
      "email": body.email
    }
  });
}

const getPostUserMissingField = (body) => {
  const userPostFields = ["name", "email", "password", "passwordConfirmation"];
  userPostFields.forEach((field) => {
    if (body[field] == undefined) return field;
  });
  return "";
}

const getPostUserWrongType = async (body, db) => {
  if (typeof(body.name) != "string" || body.name == "") return "name";

  if (
    typeof(body.email) != "string" ||
    !validEmail(body.email)||
    ! await uniqueEmail(body.email, db)
  ) return "email";

  console.log(`typeof password: ${typeof(body.password)}; passwordSize: ${body.password.length}`)
  if (
    typeof(body.password) != "string"||
    body.password.length < 8 ||
    body.password.length > 32
  ) return "password";

  if (
    typeof(body.passwordConfirmation) != "string"||
    body.passwordConfirmation.length < 8 ||
    body.passwordConfirmation.length > 32
  ) return "passwordConfirmation";
  
  return "";
}

const validEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

const passwordAndConfirmationMatch = (body) => {
  return body.password === body.passwordConfirmation;
}

const uniqueEmail = async (email, db) => {
  const users = db.collection("users");
  const result = await users.findOne({ email });
  console.log(`Result of miss findOne ${result}`);
  return result == null;
}