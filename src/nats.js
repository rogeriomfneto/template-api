const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */
    const db = mongoClient.db("CRUD");
    const users = db.collection("users");
    const subscribeUserCreated = conn.subscribe("UserCreated");
    subscribeUserCreated.on("message", handleUserCreated(users));

    const subscribeUserDeleted = conn.subscribe("UserDeleted");
    subscribeUserDeleted.on("message", handleUserDeleted(users));
  });

  return conn;
};

const handleUserCreated = (users) => (msg) => {
  console.log("Received a message [" + msg.getSequence() + "]");
  const natsPayload = JSON.parse(msg.getData());
  return users.insertOne({
    uuid: natsPayload.entityId,
    ...natsPayload.entityAggregate,
  });
}

const handleUserDeleted = (users) => async (msg) => {
  console.log("Received a message [" + msg.getSequence() + "]");
  const natsPayload = JSON.parse(msg.getData());
  const document = await users.findOne({ uuid: natsPayload.entityId });
  console.log("nats.js document", document)
  console.log("entityId", natsPayload.entityId)
  if (document) {
    console.log("nats.js deleting document")
    return users.deleteOne({
      uuid: natsPayload.entityId,
    });
  }
  console.log("nats.js document not found");
}
